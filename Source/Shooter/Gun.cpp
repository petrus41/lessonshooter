// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SkeletalMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "TimerManager.h"

// Sets default values
AGun::AGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);
}

void AGun::PullTrigger()
{
	if (bIsReloading||!ConsumeAmmo())
		return;
	UGameplayStatics::SpawnSoundAttached(MuzzleSound,Mesh, TEXT("MuzzleFlashSocket"));
	UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("MuzzleFlashSocket"));
	FVector ShootDirection;
	FHitResult Hit;
	bool bSuccess = GunTrace(Hit, ShootDirection);
	if (!bSuccess)
		return;
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(),HitSound,Hit.Location,ShootDirection.Rotation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitEffect, Hit.Location,(-ShootDirection).Rotation());
	AActor* HitActor = Hit.GetActor();

	FPointDamageEvent DamageEvent(Damage, Hit, (-ShootDirection), nullptr);
	AController* OwnerController = GetOwnerController();
	if (OwnerController == nullptr)
		return;
	HitActor->TakeDamage(Damage, DamageEvent, OwnerController, this);
}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();
	AmmoInMag = MagSize;
}

bool AGun::GunTrace(FHitResult& Hit, FVector& ShootDirection)
{
	AController* OwnerController = GetOwnerController();
	if (OwnerController == nullptr)
		return false;
	FVector Location;
	FRotator Rotation;
	OwnerController->GetPlayerViewPoint(Location, Rotation);
	ShootDirection = Rotation.Vector();
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());
	return GetWorld()->LineTraceSingleByChannel(Hit, Location, Location + (ShootDirection * 1000), ECollisionChannel::ECC_Visibility, Params);
}

AController* AGun::GetOwnerController() const
{
	APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if (OwnerPawn == nullptr)
		return nullptr;
	return  OwnerPawn->GetController();
	
}

// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGun::SetActive(bool Active)
{
	bIsActive = Active;
	SetActorHiddenInGame(!Active);
	SetActorEnableCollision(Active);
	SetActorTickEnabled(Active);
}

void AGun::StartReload()
{
	if (!bIsReloading)
	{
		bIsReloading = true;
		GetWorldTimerManager().SetTimer(ReloadTimer, this, &AGun::Reload, ReloadTime);
	}
}

void AGun::Reload()
{
	int ToUseAmmo = MagSize - AmmoInMag;
	AmmoInMag = FMath::Min(MagSize,MaxAmmo);
	MaxAmmo -= MaxAmmo >= ToUseAmmo ? ToUseAmmo : MaxAmmo;
	bIsReloading = false;
}

bool AGun::ConsumeAmmo()
{
	if (AmmoInMag > 0)
	{
		AmmoInMag--;
		return true;
	}
	else
	{
		return false;
	}
}

