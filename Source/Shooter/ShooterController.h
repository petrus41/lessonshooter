// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Blueprint/UserWidget.h"
#include "ShooterController.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API AShooterController : public APlayerController
{
	GENERATED_BODY()
public:
	virtual void GameHasEnded(class AActor* EndGameFocus , bool bIsWinner ) override;
private:

	UPROPERTY(EditAnywhere)
		float TimerDelay = 5;
	FTimerHandle RestartTimer;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> LoseScreenClass;	
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> WinScreenClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> AmmoCounter;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> ProgressBarClass;
	UUserWidget* HUD;
	UUserWidget* ProgressBar;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
