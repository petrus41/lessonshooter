// Fill out your copyright notice in the Description page of Project Settings.


#include "ZoneGameModeBase.h"
#include "EngineUtils.h"
#include "GameFramework/Controller.h"
#include "ShooterAIController.h"
#include "Kismet/GameplayStatics.h"

void AZoneGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	AActor* FoundActor = UGameplayStatics::GetActorOfClass(GetWorld(), AZone::StaticClass());
	if (FoundActor)
	{
		Zone = Cast<AZone>(FoundActor);	
		Zone->ArrayChange.AddDynamic(this, &AZoneGameModeBase::Evaluate);
		Zone->OnActorBeginOverlap.AddDynamic(this, &AZoneGameModeBase::ZoneBeginOverlap);
		Zone->OnActorEndOverlap.AddDynamic(this, &AZoneGameModeBase::ZoneEndOverlap);
	}
	GetWorldTimerManager().SetTimer(ConquerTime, this, &AZoneGameModeBase::CheckWinCon, Zone->TimeToConquer);
	GetWorldTimerManager().PauseTimer(ConquerTime);
}
void AZoneGameModeBase::CheckWinCon()
{
	if (bIsPlayerWinning)
		EndGame(true);
	else
		EndGame(false);
}
void AZoneGameModeBase::ZoneBeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	Evaluate();
}
void AZoneGameModeBase::ZoneEndOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	Evaluate();
}
void AZoneGameModeBase::EndGame(bool bIsPlayerWinner)
{
	for (AController* Controller : TActorRange<AController>(GetWorld()))
	{
		bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner;
		Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
	}
}

float AZoneGameModeBase::GetTimerPercentage()
{
	return 1-(GetWorldTimerManager().GetTimerRemaining(ConquerTime)/ Zone->TimeToConquer);
}

bool AZoneGameModeBase::GetBarColor()
{
	if(bIsPlayerWinning)
		return true;
	else
		return false;
}

void AZoneGameModeBase::Evaluate()
{
	bool bIsEnemiesIn = false;
	bool bIsPlayerIn = false;
	
	for (APawn* Pawn: Zone->PawnInZone)
	{
		if(Pawn->GetController()->IsPlayerController())
		{
			bIsPlayerIn = true;
		}
		else
		{
			bIsEnemiesIn = true;
		}
	}
	if (bIsPlayerIn != bIsEnemiesIn)
	{	
		if (bIsPlayerIn == bIsPlayerWinning )
		{
			GetWorldTimerManager().UnPauseTimer(ConquerTime);
		}
		else
		{
			GetWorldTimerManager().ClearTimer(ConquerTime);
			GetWorldTimerManager().SetTimer(ConquerTime, this, &AZoneGameModeBase::CheckWinCon, Zone->TimeToConquer);
		}
		bIsPlayerWinning = bIsPlayerIn;
	}
	else
	{
		GetWorldTimerManager().PauseTimer(ConquerTime);
	}
}
