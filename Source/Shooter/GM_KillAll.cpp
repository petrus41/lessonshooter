// Fill out your copyright notice in the Description page of Project Settings.


#include "GM_KillAll.h"
#include "EngineUtils.h"
#include "GameFramework/Controller.h"
#include "ShooterAIController.h"

void AGM_KillAll::PawnKilled(APawn* Pawn)
{
	APlayerController* Controller = Cast<APlayerController>(Pawn->GetController());
	if (Controller != nullptr)
	{
		EndGame(false);
	}
	else
	{
		for (AShooterAIController* AIController : TActorRange<AShooterAIController>(GetWorld()))
		{
			if (!AIController->IsDead())
			{
				return;
			}
		}
		EndGame(true);
	}
	
}

void AGM_KillAll::EndGame(bool bIsPlayerWinner)
{
	for (AController* Controller : TActorRange<AController>(GetWorld()))
	{
		/*if (Controller == PlayerController)
		{
			Controller->GameHasEnded(nullptr, bIsPlayerWinner);
		}
		else
		{
			Controller->GameHasEnded(nullptr, !bIsPlayerWinner);
		}*/
		bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner;
		Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
	} 

}
