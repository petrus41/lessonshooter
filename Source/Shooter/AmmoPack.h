// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AmmoPack.generated.h"

UCLASS()
class SHOOTER_API AAmmoPack : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAmmoPack();

protected:
	UPROPERTY(EditAnyWhere)
		UStaticMeshComponent* Mesh;
	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root;

public:	
	UPROPERTY(Editanywhere)
		int AmmoQuantity;
	UPROPERTY(EditAnywhere)
		bool bIsPrimaryAmmo;
};
