// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include "Gun.h"
#include "AmmoPack.h"
#include "Components/CapsuleComponent.h"
#include "ShooterGameModeBase.h"
// Sets default values
AShooterCharacter::AShooterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);
	TriggerCapsule->SetRelativeLocation(FVector(0, 0, -96.f));
	TriggerCapsule->SetCollisionProfileName(FName("Trigger"));
	TriggerCapsule->SetCapsuleSize(250.f, 250.f);

}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &AShooterCharacter::OnOverlapBegin);
	TriggerCapsule->OnComponentEndOverlap.AddDynamic(this, &AShooterCharacter::OnOverlapEnd);
	Guns.Add(GetWorld()->SpawnActor<AGun>(FirstGunClass));
	Guns.Add(GetWorld()->SpawnActor<AGun>(SecondGunClass));
	GetMesh()->HideBoneByName(TEXT("weapon"),EPhysBodyOp::PBO_None);
	Guns[0]->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("WeaponSocket"));
	Guns[1]->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("WeaponSocket"));
	Guns[0]->SetOwner(this);
	Guns[1]->SetOwner(this);
	Gun = Guns[0];
	Guns[0]->SetActive(true);
	Guns[1]->SetActive(false);
	Health = MaxHealth;
	if (!GetController()->IsPlayerController())
	{
		TriggerCapsule->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

bool AShooterCharacter::IsDead() const
{
	if (Health <= 0)
		return true;
	else
		return false;
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("LookUpPad"), this, &AShooterCharacter::LookUpRate);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis(TEXT("LookRightPad"), this, &AShooterCharacter::LookRightRate);
	PlayerInputComponent->BindAction(TEXT("Jump"),IE_Pressed,this,&ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Reload"),IE_Pressed,this,&AShooterCharacter::Reload);
	PlayerInputComponent->BindAction(TEXT("Shoot"), IE_Pressed, this, &AShooterCharacter::Shoot);
	PlayerInputComponent->BindAction(TEXT("WeaponSlot1"), IE_Pressed, this, &AShooterCharacter::ChangeWeaponTo1);
	PlayerInputComponent->BindAction(TEXT("WeaponSlot2"), IE_Pressed, this, &AShooterCharacter::ChangeWeaponTo2);
	PlayerInputComponent->BindAction(TEXT("WeaponSwitch"), IE_Pressed, this, &AShooterCharacter::SwitchWeapon);
}

float AShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	DamageToApply = FMath::Min(Health, DamageToApply);
	Health -= DamageToApply;

	UE_LOG(LogTemp, Warning, TEXT("Health left %f"), Health);
	if (IsDead())
	{
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		AShooterGameModeBase* GM = GetWorld()->GetAuthGameMode< AShooterGameModeBase>();
		if (GM != nullptr)
		{
			GM->PawnKilled(this);
		}
		DetachFromControllerPendingDestroy();
	}
	return DamageToApply;
}

void AShooterCharacter::MoveForward(float AxisValue)
{
	AddMovementInput(GetActorForwardVector() * AxisValue);
}

void AShooterCharacter::MoveRight(float AxisValue)
{
	AddMovementInput(GetActorRightVector() * AxisValue);
}

void AShooterCharacter::LookUpRate(float AxisValue)
{
	AddControllerPitchInput(GetWorld()->GetDeltaSeconds() * RotationRate * AxisValue);

}
void AShooterCharacter::LookRightRate(float AxisValue)
{
	AddControllerYawInput(GetWorld()->GetDeltaSeconds() * RotationRate * AxisValue);

}

void AShooterCharacter::Shoot()
{
	Gun->PullTrigger();
}
void AShooterCharacter::Reload()
{
	Gun->StartReload();
}
float AShooterCharacter::GetHealthPercent() const
{
	return Health / MaxHealth;
}

FString AShooterCharacter::GetAmmoString() const
{
	FString MagAmmo = FString::FromInt(Gun->AmmoInMag);
	FString MaxAmmo = FString::FromInt(Gun->MaxAmmo);
	
	FString ReturnString = MagAmmo + "/" + MaxAmmo;
	return ReturnString;
}

void AShooterCharacter::SwitchWeapon()
{
	if (Guns[0]->bIsActive)
	{
		ChangeWeaponTo2();
	}
	else
	{
		ChangeWeaponTo1();
	}
}
void AShooterCharacter::ChangeWeaponTo1()
{
	Gun = Guns[0];
	Guns[1]->SetActive(false);
	Guns[0]->SetActive(true);
}
void AShooterCharacter::ChangeWeaponTo2()
{
	Gun = Guns[1];
	Guns[0]->SetActive(false);
	Guns[1]->SetActive(true);
}

void AShooterCharacter::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AAmmoPack* Pack = Cast<AAmmoPack>(OtherActor);
	if(Pack)
	{
		if (Pack->bIsPrimaryAmmo)
		{
			Guns[0]->MaxAmmo += Pack->AmmoQuantity;
		}
		else
		{
			Guns[1]->MaxAmmo += Pack->AmmoQuantity;
		}
		Pack->Destroy();
	}

	UE_LOG(LogTemp, Warning, TEXT("Enter"));
}

void AShooterCharacter::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("Exit"));
}

