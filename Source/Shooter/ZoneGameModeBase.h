// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Zone.h"
#include "GameFramework/GameModeBase.h"
#include "ZoneGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API AZoneGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	AZone* Zone;
	void Evaluate();
	bool bIsPlayerWinning;
	FTimerHandle ConquerTime;
	void EndGame(bool bIsPlayerWinner);
	void CheckWinCon();
	UFUNCTION()
		void ZoneBeginOverlap(AActor* OverlappedActor, AActor* OtherActor);
	UFUNCTION()
		void ZoneEndOverlap(AActor* OverlappedActor, AActor* OtherActor);

public:
	UFUNCTION(BlueprintCallable)
		float GetTimerPercentage();
	UFUNCTION(BlueprintCallable)
		bool GetBarColor();

};
