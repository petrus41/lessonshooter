// Fill out your copyright notice in the Description page of Project Settings.


#include "Zone.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AZone::AZone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Root"));
	SetRootComponent(Mesh);

	Trigger = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Mesh"));
	Trigger->SetupAttachment(Mesh);
}

// Called when the game starts or when spawned
void AZone::BeginPlay()
{
	Super::BeginPlay();
	OnActorBeginOverlap.AddDynamic(this, &AZone::ZoneBeginOverlap);
	OnActorEndOverlap.AddDynamic(this, &AZone::ZoneEndOverlap);
	PawnInZone = TArray<APawn*>();
}

// Called every frame
void AZone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void AZone::ZoneBeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	APawn* Character = Cast<APawn>(OtherActor);
	if (Character)
	{
		PawnInZone.Add(Character);
		ArrayChange.Broadcast();
	}

}

void AZone::ZoneEndOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	APawn* Character = Cast<APawn>(OtherActor);
	if (Character)
	{
		PawnInZone.Remove(Character);
		ArrayChange.Broadcast();
	}

}


