// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterGameModeBase.h"
#include "GM_KillAll.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API AGM_KillAll : public AShooterGameModeBase
{
	GENERATED_BODY()
	
public:
	virtual void PawnKilled(APawn* Pawn) override;
private:
	void EndGame(bool bIsPlayerWinner);
};
