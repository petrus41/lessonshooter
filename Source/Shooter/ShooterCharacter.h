// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ShooterCharacter.generated.h"

class AGun;

UCLASS()
class SHOOTER_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShooterCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, Category = Trigger)
		UCapsuleComponent* TriggerCapsule;

public:	
	UFUNCTION(BlueprintPure)
	bool IsDead()const;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	void Shoot();
	void Reload();
	UFUNCTION(BlueprintPure)
		float GetHealthPercent() const;	
	UFUNCTION(BlueprintPure)
		FString GetAmmoString() const;

private:
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void LookUpRate(float AxisValue);
	void LookRightRate(float AxisValue);
	void SwitchWeapon();
	void ChangeWeaponTo1();
	void ChangeWeaponTo2();



	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);


	UPROPERTY(EditAnywhere)
		float RotationRate = 10;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AGun> FirstGunClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AGun> SecondGunClass;
	UPROPERTY()
		TArray<AGun*> Guns;
	UPROPERTY()
		AGun* Gun;
	UPROPERTY(EditDefaultsOnly)
		float Health;
	UPROPERTY(EditDefaultsOnly)
		float MaxHealth;
};
