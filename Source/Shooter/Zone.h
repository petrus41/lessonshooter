// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Blueprint/UserWidget.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Zone.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FClearEvent);

UCLASS()
class SHOOTER_API AZone : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AZone();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditDefaultsOnly)
		UStaticMeshComponent* Mesh;
	UPROPERTY(EditDefaultsOnly)
		UCapsuleComponent* Trigger;
	UFUNCTION()
		void ZoneBeginOverlap(AActor* OverlappedActor, AActor* OtherActor);
	UFUNCTION()
		void ZoneEndOverlap(AActor* OverlappedActor, AActor* OtherActor);
public:	
	FClearEvent ArrayChange; 
	UPROPERTY(EditAnyWhere)
		float TimeToConquer;
	TArray<APawn*> PawnInZone;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
