// Fill out your copyright notice in the Description page of Project Settings.

#include "PulseGun.h"
#include "Kismet/GameplayStatics.h"
void APulseGun::PullTrigger()
{
	Shoot();
}

void APulseGun::Shoot()
{
	if (bIsReloading || !ConsumeAmmo())
	{
		Counter = 0;
		return;
	}
	Counter++;
	UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("MuzzleFlashSocket"));
	UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("MuzzleFlashSocket"));
	FVector ShootDirection;
	FHitResult Hit;
	bool bSuccess = GunTrace(Hit, ShootDirection);
	if (Counter < NumberOfShoots)
	{
		GetWorldTimerManager().SetTimer(Burst, this, &APulseGun::Shoot, BurstTimer);
	}
	else
	{
		Counter = 0;
	}
	if (!bSuccess)
		return;
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), HitSound, Hit.Location, ShootDirection.Rotation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitEffect, Hit.Location, (-ShootDirection).Rotation());
	AActor* HitActor = Hit.GetActor();

	FPointDamageEvent DamageEvent(Damage, Hit, (-ShootDirection), nullptr);
	AController* OwnerController = GetOwnerController();
	if (OwnerController == nullptr)
		return;
	HitActor->TakeDamage(Damage, DamageEvent, OwnerController, this);
}
