// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterAIController.h"
#include "ZoneAIController.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API AZoneAIController : public AShooterAIController
{
	GENERATED_BODY()
public:
		virtual void BeginPlay() override;
};
