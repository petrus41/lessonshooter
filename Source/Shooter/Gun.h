// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Particles/ParticleSystem.h"
#include "Gun.generated.h"

UCLASS()
class SHOOTER_API AGun : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGun();

	virtual void PullTrigger();
	void StartReload();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool GunTrace(FHitResult& Hit, FVector& ShotDirection);
	AController* GetOwnerController() const;
	void Reload();
	bool ConsumeAmmo();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void SetActive(bool Active);
	bool bIsActive;
	int AmmoInMag;
	UPROPERTY(EditAnywhere)
		int MagSize;
	UPROPERTY(EditAnywhere)
		int MaxAmmo;

protected:
	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root;
	UPROPERTY(VisibleAnywhere)
		USkeletalMeshComponent* Mesh;
	UPROPERTY(EditAnywhere)
		float ReloadTime;
	UPROPERTY(EditAnywhere)
		UParticleSystem* MuzzleFlash;
	UPROPERTY(EditAnywhere)
		UParticleSystem* HitEffect;
	UPROPERTY(EditAnywhere)
		float Damage = 10;
	UPROPERTY(EditAnywhere)
		USoundBase* MuzzleSound;
	UPROPERTY(EditAnywhere)
		USoundBase* HitSound;
	FTimerHandle ReloadTimer;
	bool bIsReloading;

};
