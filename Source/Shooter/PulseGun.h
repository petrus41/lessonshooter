// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gun.h"
#include "PulseGun.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API APulseGun : public AGun
{
	GENERATED_BODY()
public:
	virtual void PullTrigger() override;
protected:
	UPROPERTY(EditAnywhere)
		float BurstTimer;
	UPROPERTY(EditAnywhere)
		int NumberOfShoots;
	int Counter;
	bool bIsFireing;
	FTimerHandle Burst;
	void Shoot();
};
