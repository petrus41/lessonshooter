// Fill out your copyright notice in the Description page of Project Settings.

#include "ZoneAIController.h"
#include "Zone.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
void AZoneAIController::BeginPlay()
{
	Super::BeginPlay();
	AActor* FoundActor = UGameplayStatics::GetActorOfClass(GetWorld(), AZone::StaticClass());
	GetBlackboardComponent()->SetValueAsVector(TEXT("ZoneLocation"), FoundActor->GetActorLocation());
}

