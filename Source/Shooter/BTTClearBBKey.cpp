// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTClearBBKey.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTTClearBBKey::UBTTClearBBKey()
{
	NodeName = TEXT("Clear Board Value");
}


EBTNodeResult::Type UBTTClearBBKey::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	OwnerComp.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey());
	return EBTNodeResult::Succeeded;

}

