// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterController.h"
#include "TimerManager.h"


void AShooterController::GameHasEnded(class AActor* EndGameFocus = nullptr, bool bIsWinner = false)
{
	Super::GameHasEnded(EndGameFocus, bIsWinner);
	if (bIsWinner)
	{
		UUserWidget* WinScreen = CreateWidget(this, WinScreenClass);
		if (WinScreen != nullptr)
		{
			WinScreen->AddToViewport();
		}
	}
	else
	{
		UUserWidget* LoseScreen = CreateWidget(this,LoseScreenClass);
		if (LoseScreen != nullptr)
		{
			LoseScreen->AddToViewport();
		}
	}
	HUD->RemoveFromViewport();
	GetWorldTimerManager().SetTimer(RestartTimer, this, &APlayerController::RestartLevel, TimerDelay);
}

void AShooterController::BeginPlay()
{
	Super::BeginPlay();
	HUD = CreateWidget(this, AmmoCounter);
	ProgressBar = CreateWidget(this, ProgressBarClass);
	if (HUD)
	{
		HUD->AddToViewport();
	}
	if (ProgressBar)
	{
		ProgressBar->AddToViewport();
	}
}
